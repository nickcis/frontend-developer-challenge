const path = require('path');

module.exports = {
  modify(config, { target, dev }) {
    config.resolve = {
      ...config.resolve,
      alias: {
        ...config.resolve.alias,
        Common: path.resolve(__dirname, 'src/common'),
        Client: path.resolve(__dirname, 'src/client'),
        Server: path.resolve(__dirname, 'src/server'),
      },
    };
    if (!dev) {
      // Disabling performance hints, the scope of this challenge isn't to optimize on web perf
      config.performance = {
        hints: false
      };
    }

    return config;
  }
};
