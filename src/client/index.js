import App from '../common/App';
import { BrowserRouter } from 'react-router-dom';
import React from 'react';
import { hydrate } from 'react-dom';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { ApolloProvider } from 'react-apollo';
import theme from '../common/theme';
import client from './apollo';
import CartProvider from 'Common/components/CartProvider';

hydrate(
  <MuiThemeProvider theme={theme}>
    <ApolloProvider client={client}>
      <CartProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </CartProvider>
    </ApolloProvider>
  </MuiThemeProvider>,
  document.getElementById('root'),
  () => {
    const jssStyles = document.getElementById('jss-server-side');
    if (jssStyles && jssStyles.parentNode)
      jssStyles.parentNode.removeChild(jssStyles);
  }
);

if (module.hot) {
  module.hot.accept();
}
