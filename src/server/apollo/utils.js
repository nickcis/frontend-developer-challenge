/** Compara dos ids para ver si son iguales.
 * Resuelve el problema de que graphQl castea los ids a string.
 *
 * @param {String/Number} a - id
 * @param {String/Number} b - id
 * @return {Boolean} true: si son iguales
 */
export function areIdsEqual(a, b) {
  return `${a}` === `${b}`;
}

/** Busca una subcategoria
 *
 * @param {Category[]} categories - lista de categorias
 * @param {String/Number} sublevelId - id de sublevel
 * @return {Category} - sublevel del id o `undefined`
 */
export function findSublevelInCategories(categories, sublevelId) {
  function _findInCategories(sublevels) {
    let sublevel;

    sublevels.some(level => {
      if (areIdsEqual(level.id, sublevelId)) {
        sublevel = level;
        return true;
      }

      if (level.sublevels) {
        sublevel = _findInCategories(level.sublevels);
        return sublevel;
      }

      return false;
    });

    return sublevel;
  }

  let level;

  categories.some(category => {
    return (level = _findInCategories(category.sublevels || []));
  });

  return level;
}
