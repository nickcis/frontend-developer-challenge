import categories from '../../../categories.json';
import products from '../../../products.json';
import { areIdsEqual, findSublevelInCategories } from './utils';

products.products = products.products.map(product => ({
  ...product,
  price: parseFloat(product.price.substring(1).replace(',', '')),
}));

const resolvers = {
  Query: {
    categories() {
      return categories.categories;
    },
    sublevel(parent, args) {
      return findSublevelInCategories(categories.categories, args.id);
    },
    product(parent, args) {
      return products.products.find(({ id }) => areIdsEqual(id, args.id));
    },
    products(parent, args) {
      let prod = products.products;

      if (args.sublevel) {
        prod = prod.filter((
          { sublevel_id } // eslint-disable-line camelcase
        ) => areIdsEqual(sublevel_id, args.sublevel));
      }

      if (args.query) {
        const query = args.query.trim().toLowerCase();
        try {
          const reg = new RegExp(query, 'i');
          prod = prod.filter(({ name }) => reg.test(name));
        } catch (e) {
          prod = prod.filter(({ name }) => name.toLowerCase().includes(query));
        }
      }

      if ('available' in args) {
        prod = prod.filter(({ available }) => !!available === !!args.available);
      }

      if (Number.isInteger(args.quantity)) {
        prod = prod.filter(({ quantity }) => quantity === args.quantity);
      }

      const min = parseFloat(args.min);
      if (!isNaN(min)) {
        prod = prod.filter(({ price }) => min <= price);
      }

      const max = parseFloat(args.max);
      if (!isNaN(max)) {
        prod = prod.filter(({ price }) => price <= max);
      }

      switch (args.order) {
        case 'PRICE_ASC':
          prod.sort(({ price: a }, { price: b }) => a - b);
          break;
        case 'PRICE_DESC':
          prod.sort(({ price: a }, { price: b }) => b - a);
          break;
        case 'QUANTITY_ASC':
          console.log('asd');
          prod.sort(({ quantity: a }, { quantity: b }) => a - b);
          break;
        case 'QUANTITY_DESC':
          prod.sort(({ quantity: a }, { quantity: b }) => b - a);
          break;
        case 'AVAILABILITY_ASC':
          prod.sort(({ available: a }, { available: b }) => a - b);
          break;
        case 'AVAILABILITY_DESC':
          prod.sort(({ available: a }, { available: b }) => b - a);
          break;
        default:
          break;
      }

      return prod;
    },
  },
  Mutation: {
    finishCart() {
      return 200;
    },
  },
  Product: {
    sublevel(product) {
      return resolvers.Query.sublevel(null, { id: product.sublevel_id });
    },
  },
};

export default resolvers;
