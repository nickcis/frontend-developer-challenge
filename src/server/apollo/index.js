import { ApolloServer, gql } from 'apollo-server-express';
import resolvers from './resolvers';

const typeDefs = gql`
  type Sublevel {
    id: ID!
    name: String!
    sublevels: [Sublevel]
  }

  type Category {
    id: ID!
    name: String!
    sublevels: [Sublevel]
  }

  type Product {
    id: ID!
    quantity: Int!
    available: Boolean!
    price: Float!
    sublevel: Sublevel
    name: String!
  }

  input ItemInput {
    id: ID!
    amount: Int!
  }

  enum ProductOrder {
    PRICE_ASC
    PRICE_DESC
    QUANTITY_ASC
    QUANTITY_DESC
    AVAILABILITY_ASC
    AVAILABILITY_DESC
  }

  type Query {
    categories: [Category]
    sublevel(id: ID!): Sublevel
    products(
      sublevel: ID
      query: String
      available: Boolean
      quantity: Int
      min: Float
      max: Float
      order: ProductOrder
    ): [Product]
    product(id: ID!): Product
  }

  type Mutation {
    finishCart(items: [ItemInput]!): Int
  }
`;

const apolloServer = new ApolloServer({ typeDefs, resolvers });

export default apolloServer;
