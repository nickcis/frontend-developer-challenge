import { areIdsEqual, findSublevelInCategories } from './utils';

describe('areIdsEqual', () => {
  it.each([
    [1, 1, true],
    [1, '1', true],
    ['1', '1', true],
    [0, '', false],
    [1, 2, false],
    ['1', 2, false],
  ])('%j === %j ? %s', (a, b, expected) => {
    expect(areIdsEqual(a, b)).toBe(expected);
    expect(areIdsEqual(b, a)).toBe(expected);
  });
});

describe('findSublevelInCategories', () => {
  it.each([
    ['Should ignore higher level', [{ id: 1 }], 1, undefined],
    [
      'Should find sublevel in first level',
      [{ sublevels: [{ id: 1 }] }],
      1,
      { id: 1 },
    ],
    [
      'Should find sublevel in second level',
      [{ sublevels: [{ sublevels: [{ id: 1 }] }] }],
      1,
      { id: 1 },
    ],
    [
      'Should find sublevel using id as string',
      [{ sublevels: [{ id: 1 }] }],
      '1',
      { id: 1 },
    ],
    [
      'Should find sublevel in second category',
      [{}, { sublevels: [{ id: 1 }] }],
      '1',
      { id: 1 },
    ],
  ])('%s', (_, categories, id, expected) => {
    expect(findSublevelInCategories(categories, id)).toEqual(expected);
  });
});
