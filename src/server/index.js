import apollo from './apollo';
import App from '../common/App';
import React from 'react';
import { StaticRouter } from 'react-router-dom';
import express from 'express';
import { renderToString } from 'react-dom/server';
import { SheetsRegistry } from 'jss';
import JssProvider from 'react-jss/lib/JssProvider';
import {
  MuiThemeProvider,
  createGenerateClassName,
} from '@material-ui/core/styles';
import { ApolloProvider, getDataFromTree } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { SchemaLink } from 'apollo-link-schema';
import serialize from 'serialize-javascript';
import theme from '../common/theme';

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);

const server = express();

apollo.applyMiddleware({ app: server });

server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', (req, res) => {
    const client = new ApolloClient({
      ssrMode: true,
      link: new SchemaLink({
        schema: apollo.schema,
      }),
      cache: new InMemoryCache(),
    });
    const sheetsRegistry = new SheetsRegistry();
    const sheetsManager = new Map();
    const generateClassName = createGenerateClassName();
    const context = {};
    const app = (
      <JssProvider
        registry={sheetsRegistry}
        generateClassName={generateClassName}
      >
        <MuiThemeProvider theme={theme} sheetsManager={sheetsManager}>
          <ApolloProvider client={client}>
            <StaticRouter context={context} location={req.url}>
              <App />
            </StaticRouter>
          </ApolloProvider>
        </MuiThemeProvider>
      </JssProvider>
    );

    getDataFromTree(
      <ApolloProvider client={client}>
        <StaticRouter context={context} location={req.url}>
          <App />
        </StaticRouter>
      </ApolloProvider>
    ).then(() => {
      const markup = renderToString(app);
      const css = sheetsRegistry.toString();
      const initialState = client.extract();

      if (context.url) {
        res.redirect(context.url);
      } else {
        res.status(200).send(
          `<!doctype html>
        <html lang="">
        <head>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta charSet='utf-8' />
            <title>Bienvenido a: El Barat&oacute;n!</title>
            <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
            ${
              assets.client.css
                ? `<link rel="stylesheet" href="${assets.client.css}">`
                : ''
            }
            ${css ? `<style id="jss-server-side">${css}</style>` : ''}
        </head>
        <body>
            <div id="root">${markup}</div>
            <script>
              window.__APOLLO_STATE__ = ${serialize(initialState)}
            </script>
            ${
              process.env.NODE_ENV === 'production'
                ? `<script src="${assets.client.js}" defer></script>`
                : `<script src="${
                    assets.client.js
                  }" defer crossorigin></script>`
            }
        </body>
</ht    ml>`
        );
      }
    });
  });

export default server;
