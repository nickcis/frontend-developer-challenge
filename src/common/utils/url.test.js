import { Routes, buildURL } from './url';

describe('buildURL', () => {
  describe('Home', () => {
    it.each([
      ['should build url', [], '/'],
      ['should build url with category', [{ category: '1' }], '/?category=1'],
      [
        'should build url with category & sublevels',
        [{ category: '1', sublevels: [1, 2, 3] }],
        '/?category=1&sublevels=1,2,3',
      ],
      [
        'should ignore sublevels if no category',
        [{ sublevels: [1, 2, 3] }],
        '/',
      ],
      ,
      [
        'should ignore sublevels if empty',
        [{ category: '1', sublevels: [] }],
        '/?category=1',
      ],
    ])('%s', (_, args, expected) => {
      expect(buildURL(Routes.HOME, ...args)).toBe(expected);
    });
  });

  describe('Listing', () => {
    it.each([
      ['should build url', [], '/items'],
      [
        'should build url with sublevel',
        [{ sublevel: { name: 'Bebidas', id: '1' } }],
        '/bebidas_sl1',
      ],
      [
        'should build url with sublevel',
        [{ sublevel: { name: 'Bebidas', id: '1' }, query: 'texto a buscar' }],
        '/bebidas_sl1?q=texto%20a%20buscar',
      ],
      [
        'should build url with query',
        [{ query: 'texto a buscar' }],
        '/items?q=texto%20a%20buscar',
      ],
      [
        'should build url with min & max',
        [{ min: '123.3', max: 124.3 }],
        '/items?min=123.3&max=124.3',
      ],
      [
        'should ignore min & max if not float',
        [{ min: 'asd123', max: '' }],
        '/items',
      ],
      [
        'should build url with quantity',
        [{ quantity: 123 }],
        '/items?quantity=123',
      ],
    ])('%s', (_, args, expected) => {
      expect(buildURL(Routes.LISTING, ...args)).toBe(expected);
    });
  });
});
