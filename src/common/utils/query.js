import React from 'react';
import Error from 'Common/components/Error';
import Loading from 'Common/components/Loading';

/** Decora funcion de render para manejar estados de loading y error de manera generica
 * @param {Function} render - Funcion de render llamada cuando no hay loading ni error
 * @return {Function} render function
 */
export function handler(render) {
  return function(args) {
    if (args.loading) return <Loading />;
    if (args.error) return <Error />;

    return render(args);
  };
}
