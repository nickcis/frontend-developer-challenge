export const Routes = {
  HOME: 'home',
  LISTING: 'listing',
  CART: 'cart',
};

export function slugify(text = '') {
  return text
    .toLowerCase()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/\s/g, '-')
    .replace(/[^-a-zA-Z0-9]/g, '');
}

/** Crea una url a partir de las opciones
 *
 * @param {Routes} route - Ruta: Home / Listing / Cart
 * @param {Object} options -
 * @return {String} url
 */
export function buildURL(route, options = {}) {
  switch (route) {
    case Routes.HOME: {
      let url = '/';
      if (options.category) {
        url += `?category=${options.category}`;

        if (Array.isArray(options.sublevels) && options.sublevels.length) {
          url += `&sublevels=${options.sublevels.join(',')}`;
        }
      }

      return url;
    }

    case Routes.LISTING: {
      let url =
        '/' +
        (options.sublevel
          ? `${slugify(options.sublevel.name)}_sl${options.sublevel.id}`
          : 'items');

      const params = [];

      if (options.query) {
        params.push(`q=${encodeURIComponent(options.query)}`);
      }

      if ('available' in options) {
        params.push(
          `available=${options.available && options.available !== 'false'}`
        );
      }

      ['min', 'max'].forEach(opt => {
        const value = parseFloat(options[opt]);
        if (!isNaN(value)) {
          params.push(`${opt}=${options[opt]}`);
        }
      });

      if (!isNaN(parseInt(options.quantity, 10))) {
        params.push(`quantity=${options.quantity}`);
      }

      if (options.order) {
        params.push(`order=${options.order}`);
      }

      if (params.length) {
        url += `?${params.join('&')}`;
      }

      return url;
    }

    case Routes.CART:
      return '/cart';

    default:
      return '';
  }
}
