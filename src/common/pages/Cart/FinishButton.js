import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';

const styles = theme => ({
  wrapper: {
    margin: theme.spacing.unit,
    position: 'relative',
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

const FinishButton = ({
  classes,
  loading,
  className,
  success,
  children,
  ...props
}) => (
  <div className={classNames(classes.wrapper, className)}>
    <Button
      variant="contained"
      color="primary"
      className={classNames({
        [classes.buttonSuccess]: success,
      })}
      disabled={loading}
      {...props}
    >
      {children}
      {loading && (
        <CircularProgress size={24} className={classes.buttonProgress} />
      )}
    </Button>
  </div>
);

FinishButton.propTypes = {
  classes: PropTypes.object.isRequired,
  loading: PropTypes.bool,
  className: PropTypes.string,
  success: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

export default withStyles(styles)(FinishButton);
