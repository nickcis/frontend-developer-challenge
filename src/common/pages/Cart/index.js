import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { handler } from 'Common/utils/query';
import useCart from 'Common/hooks/useCart';
import Empty from 'Common/components/Empty';
import Item from './Item';
import FinishButton from './FinishButton';

const GET_PRODUCT = gql`
  query Product($id: ID!) {
    product(id: $id) {
      id
      quantity
      available
      price
      name
      sublevel {
        id
        name
      }
    }
  }
`;

const FINISH_CART = gql`
  mutation FinishCart($items: [ItemInput]!) {
    finishCart(items: $items)
  }
`;

const styles = theme => ({
  container: {
    flex: '1',
    marginTop: theme.spacing.unit * 2,
  },
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    margin: '0 auto',
    [theme.breakpoints.down('xs')]: {
      margin: `0 ${theme.spacing.unit}px`,
    },
  },
  button: {
    textAlign: 'right',
  },
});

const Cart = ({ classes }) => {
  const { cart, addItem, removeItem, clear } = useCart();

  return (
    <Grid container spacing={8} className={classes.container}>
      <Grid className={classes.wrapper} xs={12} sm={11} md={10} lg={8} item>
        <Typography variant="h2" component="h1" gutterBottom>
          Carrito
        </Typography>
        {cart.length ? (
          cart.map(item => (
            <Query
              key={item.id}
              query={GET_PRODUCT}
              variables={{ id: item.id }}
            >
              {handler(({ data }) => (
                <Item
                  product={data.product}
                  amount={item.amount}
                  addItem={addItem}
                  removeItem={removeItem}
                />
              ))}
            </Query>
          ))
        ) : (
          <Empty Icon={RemoveShoppingCartIcon}>
            Tu carrito est&aacute; vac&iacute;o.
          </Empty>
        )}
        <Mutation
          mutation={FINISH_CART}
          variables={{ items: cart }}
          onCompleted={clear}
        >
          {(finishCart, { loading, error, data }) => (
            <div className={classes.button}>
              <FinishButton
                loading={loading}
                success={data && data.finishCart === 200}
                onClick={finishCart}
              >
                Terminar
              </FinishButton>
            </div>
          )}
        </Mutation>
      </Grid>
    </Grid>
  );
};

Cart.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Cart);
