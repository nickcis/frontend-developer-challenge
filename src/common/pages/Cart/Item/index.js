import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import withStyles from '@material-ui/core/styles/withStyles';
import ItemComponent from 'Common/components/Item';

const styles = theme => ({
  textField: {
    width: '3em',
    '& input': {
      textAlign: 'center',
    },
  },
});

const Item = ({ classes, product, amount, addItem, removeItem }) => (
  <ItemComponent product={product}>
    <IconButton color="primary" onClick={() => removeItem(product.id)}>
      <RemoveIcon />
    </IconButton>
    <TextField value={amount} margin="dense" className={classes.textField} />
    <IconButton color="primary" onClick={() => addItem(product.id)}>
      <AddIcon />
    </IconButton>
  </ItemComponent>
);

Item.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
  amount: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
  addItem: PropTypes.func.isRequired,
  removeItem: PropTypes.func.isRequired,
};

export default withStyles(styles)(Item);
