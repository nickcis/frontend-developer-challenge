import React from 'react';
import { render, cleanup } from 'react-testing-library';

const mockApollo = {
  Query: jest.fn(() => null),
};

jest.mock('react-apollo', () => mockApollo);

const { Home } = require('.');

describe('Home', () => {
  afterEach(() => {
    cleanup();
  });

  it('should render without exploding', () => {
    render(<Home classes={{}} location={{ search: '' }} />);
  });
});
