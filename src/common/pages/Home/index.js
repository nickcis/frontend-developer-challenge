import React from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';
import queryString from 'query-string';
import Hero from './Hero';
import Category from './Category';
import { handler } from 'Common/utils/query';

const GET_CATEGORIES = gql`
  query {
    categories {
      id
      name
      sublevels {
        id
        name
      }
    }
  }
`;

const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
});

export const Home = ({ classes, location: { search } }) => {
  const query = queryString.parse(search);

  return (
    <React.Fragment>
      <Hero />
      <div className={classNames(classes.layout, classes.cardGrid)}>
        <Grid container spacing={8}>
          <Query query={GET_CATEGORIES}>
            {handler(({ data }) =>
              data.categories.map(category => (
                <Category
                  key={category.id}
                  category={category}
                  selectedCategory={query.category}
                  selectedSublevels={(query.sublevels || '').split(',')}
                />
              ))
            )}
          </Query>
        </Grid>
      </div>
    </React.Fragment>
  );
};

Home.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
};

export default withStyles(styles)(Home);
