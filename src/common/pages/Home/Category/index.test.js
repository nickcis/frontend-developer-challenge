import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';
import { MemoryRouter, StaticRouter } from 'react-router-dom';

const mockSublevels = jest.fn();

jest.mock('./Sublevels', () => mockSublevels);

const { Category } = require('.');

describe('Category', () => {
  beforeEach(() => {
    cleanup();
    mockSublevels.mockReset().mockImplementation(() => null);
  });

  it('should render without exploding', () => {
    render(
      <MemoryRouter>
        <Category classes={{}} category={{ id: '', name: '' }} />
      </MemoryRouter>
    );
  });

  it('should change url when clicking on image', () => {
    const context = {};
    const mockCategory = {
      id: '1',
      name: 'my nice category',
    };
    const { getByTestId } = render(
      <StaticRouter context={context}>
        <Category classes={{}} category={mockCategory} />
      </StaticRouter>
    );

    const link = getByTestId('category-link');
    fireEvent.click(link);

    expect(context.url).toBe('/?category=1');
  });

  it('should render sublevels if category is selected', () => {
    const mockCategory = {
      id: '1',
      name: 'my nice category',
      sublevels: [{ id: '1', name: 'test' }],
    };
    render(
      <MemoryRouter>
        <Category classes={{}} category={mockCategory} selectedCategory="1" />
      </MemoryRouter>
    );

    expect(mockSublevels).toHaveBeenCalledWith(
      expect.objectContaining({
        selectedCategory: '1',
        sublevel: expect.objectContaining({
          sublevels: mockCategory.sublevels,
        }),
      }),
      expect.anything()
    );
  });
});
