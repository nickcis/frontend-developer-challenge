import React from 'react';
import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';

const styles = theme => ({
  grid: {
    '&:nth-last-child(n+4)': {
      display: 'none',
    },
    [theme.breakpoints.down('sm')]: {
      '&:nth-last-child(n+3)': {
        display: 'none',
      },
    },
    [theme.breakpoints.down('xs')]: {
      '&:nth-last-child(n+2)': {
        display: 'none',
      },
    },
  },
});

const Section = ({ className, classes, ...props }) => (
  <Grid
    item
    className={classNames(classes.grid, className)}
    xs={12}
    sm={6}
    md={4}
    lg={4}
    {...props}
  />
);

export default withStyles(styles)(Section);
