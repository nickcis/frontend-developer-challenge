import React from 'react';
import PropTypes from 'prop-types';
import ErrorIcon from '@material-ui/icons/Error';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Section from './Section';

const styles = {
  section: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
};

const Error = ({ classes }) => (
  <Section className={classes.section}>
    <ErrorIcon />
    <Typography>
      No se pudo cargar, vuelva a intentar m&aacute;s tarde.
    </Typography>
  </Section>
);

Error.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Error);
