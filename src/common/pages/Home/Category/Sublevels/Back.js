import React from 'react';
import PropTypes from 'prop-types';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import withStyles from '@material-ui/core/styles/withStyles';
import { Link } from 'react-router-dom';

const styles = {
  link: {
    textDecoration: 'none',
  },
};

const Back = ({ classes, to }) => (
  <React.Fragment>
    <Link key="back" className={classes.link} to={to}>
      <ListItem button>
        <ListItemIcon>
          <KeyboardArrowLeft />
        </ListItemIcon>
        <ListItemText primary="Atr&aacute;s" />
      </ListItem>
    </Link>
    <Divider key="divider" />
  </React.Fragment>
);

Back.propTypes = {
  classes: PropTypes.object.isRequired,
  to: PropTypes.string.isRequired,
};

export default withStyles(styles)(Back);
