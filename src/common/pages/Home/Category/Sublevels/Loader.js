import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import withStyles from '@material-ui/core/styles/withStyles';
import Section from './Section';

const styles = {
  section: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
};

const Loader = ({ classes }) => (
  <Section className={classes.section}>
    <CircularProgress />
  </Section>
);

Loader.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Loader);
