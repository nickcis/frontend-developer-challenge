import React, { useState } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';
import Section from './Section';
import { Routes, buildURL } from 'Common/utils/url';
import Back from './Back';
import useRouter from 'Common/hooks/useRouter';

const styles = theme => ({
  section: {
    margin: `${theme.spacing.unit}px 0`,
  },
  form: {
    margin: theme.spacing.unit * 2,
  },
  button: {
    width: '100%',
    marginTop: theme.spacing.unit,
  },
});

const Form = ({ sublevel, classes, backURL }) => {
  const [query, setQuery] = useState('');
  const router = useRouter();

  return (
    <Section className={classes.section}>
      {backURL && <Back to={backURL} />}
      <form
        onSubmit={event => {
          event.preventDefault();
          const url = buildURL(Routes.LISTING, {
            sublevel,
            query,
          });

          router.history.push(url);
        }}
        className={classes.form}
        action={buildURL(Routes.LISTING, { sublevel })}
        autoComplete="off"
      >
        <TextField
          fullWidth
          value={query}
          name="q"
          label="B&uacute;squeda"
          variant="outlined"
          margin="dense"
          onChange={event => setQuery(event.target.value)}
        />
        <Button
          className={classes.button}
          color="primary"
          variant="contained"
          type="submit"
        >
          Buscar
        </Button>
      </form>
    </Section>
  );
};

Form.propTypes = {
  sublevel: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  classes: PropTypes.object.isRequired,
  backURL: PropTypes.string,
};

export default withStyles(styles)(Form);
