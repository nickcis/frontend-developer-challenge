import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';
import { MemoryRouter, StaticRouter } from 'react-router-dom';
import Form from './Form';

describe('Form', () => {
  afterEach(cleanup);

  it('should render without exploding', () => {
    render(
      <MemoryRouter>
        <Form sublevel={{ id: '1', name: 'name' }} classes={{}} />
      </MemoryRouter>
    );
  });

  it('should redirect to listing when something is searched', () => {
    const context = {};
    const text = 'search';
    const { container } = render(
      <StaticRouter context={context}>
        <Form sublevel={{ id: '1', name: 'name' }} classes={{}} />
      </StaticRouter>
    );
    const input = container.querySelector('input[name="q"]');
    fireEvent.change(input, { target: { value: text } });

    const button = container.querySelector('[type="submit"]');
    fireEvent.click(button);

    expect(context.url).toBe('/name_sl1?q=search');
  });
});
