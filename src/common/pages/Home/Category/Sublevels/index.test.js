import React from 'react';
import { render, cleanup } from 'react-testing-library';
import { MemoryRouter } from 'react-router-dom';

const mockApollo = {
  Query: jest.fn(() => null),
};

jest.mock('react-apollo', () => mockApollo);

const { Sublevels } = require('.');

describe('Sublevels', () => {
  afterEach(() => {
    cleanup();
  });

  it('should render without exploding', () => {
    render(
      <MemoryRouter>
        <Sublevels
          selectedCategory="1"
          classes={{}}
          sublevel={{
            id: '1',
            name: 'name',
          }}
        />
      </MemoryRouter>
    );
  });
});
