import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';
import withStyles from '@material-ui/core/styles/withStyles';
import { Routes, buildURL } from 'Common/utils/url';
import Section from './Section';
import Back from './Back';
import Form from './Form';
import Loader from './Loader';
import Error from './Error';

const GET_SUBLEVEL = gql`
  query Sublevel($id: ID!) {
    sublevel(id: $id) {
      id
      name
      sublevels {
        id
        name
      }
    }
  }
`;

const styles = {
  link: {
    textDecoration: 'none',
  },
};

export const Sublevels = ({
  classes,
  sublevel: parent,
  selectedCategory,
  selectedSublevels = [],
}) => {
  let selectedSublevel;
  const sublevels = parent.sublevels;
  const parentIndex = selectedSublevels.indexOf(parent.id);
  const selectedSublevelsWithParent = selectedSublevels.slice(
    0,
    parentIndex + 1
  );
  const back = buildURL(Routes.HOME, {
    category: selectedCategory,
    sublevels: selectedSublevels.slice(0, parentIndex),
  });

  if (!sublevels) return <Form sublevel={parent} backURL={back} />;

  const elements = [
    <Section key="1">
      <List>
        {parent.id && <Back to={back} />}
        {sublevels.map(sublevel => {
          const isSelected = selectedSublevels.includes(sublevel.id);
          const sublevels =
            parentIndex >= 0
              ? [...selectedSublevelsWithParent, sublevel.id]
              : [sublevel.id];

          if (isSelected) {
            selectedSublevel = sublevel;
          }

          return (
            <Link
              key={sublevel.id}
              className={classes.link}
              to={buildURL(Routes.HOME, {
                category: selectedCategory,
                sublevels: sublevels,
              })}
            >
              <ListItem selected={isSelected} button>
                <Avatar>{sublevel.name.charAt(0)}</Avatar>
                <ListItemText primary={sublevel.name} />
              </ListItem>
            </Link>
          );
        })}
      </List>
    </Section>,
  ];

  if (selectedSublevel) {
    elements.push(
      <Query
        key="2"
        query={GET_SUBLEVEL}
        variables={{ id: selectedSublevel.id }}
      >
        {({ loading, error, data }) => {
          if (loading) return <Loader />;
          if (error) return <Error />;
          return (
            <StyledSublevels
              sublevel={data.sublevel}
              selectedCategory={selectedCategory}
              selectedSublevels={selectedSublevels}
            />
          );
        }}
      </Query>
    );
  }

  return elements;
};

Sublevels.propTypes = {
  sublevel: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    sublevels: PropTypes.array,
  }).isRequired,
  selectedCategory: PropTypes.string.isRequired,
  selectedSublevels: PropTypes.array,
};

const StyledSublevels = withStyles(styles)(Sublevels);

export default StyledSublevels;
