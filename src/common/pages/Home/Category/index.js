import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import CardMedia from '@material-ui/core/CardMedia';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import CardContent from '@material-ui/core/CardContent';
import Collapse from '@material-ui/core/Collapse';
import Sublevels from './Sublevels';
import { Routes, buildURL } from 'Common/utils/url';

const styles = theme => ({
  container: {
    transition: theme.transitions.create('max-width', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  resetLink: {
    textDecoration: 'none',
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardMediaOpen: {
    height: 200,
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
});

export const Category = ({
  classes,
  category,
  selectedCategory,
  selectedSublevels,
}) => {
  const isSelected = selectedCategory === category.id;

  return (
    <Grid
      item
      className={classes.container}
      xs={12}
      sm={isSelected ? 12 : 6}
      md={isSelected ? 12 : 4}
      lg={isSelected ? 12 : 4}
    >
      <Card className={classes.card}>
        <Link
          data-testid="category-link"
          className={classes.resetLink}
          to={buildURL(
            Routes.HOME,
            isSelected || {
              category: category.id,
            }
          )}
        >
          <CardHeader
            avatar={
              <Avatar className={classes.avatar}>
                {category.name.charAt(0)}
              </Avatar>
            }
            title={category.name}
            action={
              <IconButton
                className={classNames(classes.expand, {
                  [classes.expandOpen]: isSelected,
                })}
              >
                <ExpandMoreIcon />
              </IconButton>
            }
          />
          <CardMedia
            className={classNames({
              [classes.cardMediaOpen]: isSelected,
              [classes.cardMedia]: !isSelected,
            })}
            image={`https://loremflickr.com/320/240/${category.name}`}
            title={category.name}
          />
        </Link>
        <Collapse in={isSelected} timeout="auto" unmountOnExit>
          <CardContent>
            <Grid container>
              <Sublevels
                sublevel={{ sublevels: category.sublevels }}
                selectedSublevels={selectedSublevels}
                selectedCategory={selectedCategory}
              />
            </Grid>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  );
};

Category.propTypes = {
  classes: PropTypes.object.isRequired,
  category: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  selectedCategory: PropTypes.string,
  selectedSublevels: PropTypes.array,
};

export default withStyles(styles)(Category);
