import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  heroUnit: {
    backgroundColor: theme.palette.background.paper,
  },
  heroContent: {
    maxWidth: 600,
    margin: '0 auto',
    padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
  },
});

const Hero = ({ classes }) => (
  <div className={classes.heroUnit}>
    <div className={classes.heroContent}>
      <Typography
        component="h1"
        variant="h2"
        align="center"
        color="textPrimary"
        gutterBottom
      >
        El Barat&oacute;n
      </Typography>
      <Typography variant="h6" align="center" color="textSecondary" paragraph>
        La mejor tienda para comprar bueno, bonito y barato. Y ahora, ¡desde el
        alcance de tu mano! Disfruta todos los productos ordenados en las
        mejores categor&iacute;as.
      </Typography>
    </div>
  </div>
);

Hero.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Hero);
