import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = {
  title: {
    textTransform: 'capitalize',
  },
};

const Title = ({ classes, variables, sublevel }) => (
  <Typography
    variant="h2"
    component="h1"
    className={classes.title}
    gutterBottom
  >
    {variables.query || sublevel.name}
  </Typography>
);

Title.propTypes = {
  classes: PropTypes.object.isRequired,
  variables: PropTypes.object.isRequired,
  sublevel: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(Title);
