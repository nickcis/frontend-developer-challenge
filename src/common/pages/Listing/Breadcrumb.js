import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { Routes, buildURL } from 'Common/utils/url';

const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'flex-start',
    margin: `${theme.spacing.unit}px 0`,
  },
  text: {
    margin: `0 ${theme.spacing.unit}px`,
    '&:first-child': {
      marginLeft: 0,
    },
    textDecoration: 'none',
  },
});

const Separator = () => (
  <Typography key="separator" variant="caption">
    &gt;
  </Typography>
);

const Breadcrumb = ({ classes, sublevel, variables = {} }) => {
  let separators = 0;
  const breadcrumbs = [
    <Typography
      key="home"
      component={Link}
      className={classes.text}
      variant="caption"
      to={buildURL(Routes.HOME)}
    >
      Inicio
    </Typography>,
    <Separator key={`sep-${separators++}`} />,
    <Typography key="dots" className={classes.text} variant="caption">
      ...
    </Typography>,
    <Separator key={`sep-${separators++}`} />,
    <Typography
      key={sublevel.id}
      component={Link}
      className={classes.text}
      variant="caption"
      to={buildURL(Routes.LISTING, { sublevel })}
    >
      {sublevel.name}
    </Typography>,
  ];

  if (variables.query) {
    breadcrumbs.push(<Separator key={`sep-${separators++}`} />);
    breadcrumbs.push(
      <Typography
        key={`query-${variables.query}`}
        component={Link}
        className={classes.text}
        variant="caption"
        to={buildURL(Routes.LISTING, { sublevel, query: variables.query })}
      >
        {variables.query}
      </Typography>
    );
  }

  return <div className={classes.container}>{breadcrumbs}</div>;
};

Breadcrumb.propTypes = {
  classes: PropTypes.object.isRequired,
  sublevel: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
  variables: PropTypes.object,
};

export default withStyles(styles)(Breadcrumb);
