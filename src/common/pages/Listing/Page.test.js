import React from 'react';
import { render, cleanup } from 'react-testing-library';
import { MemoryRouter } from 'react-router-dom';

const { Page } = require('./Page');

describe('Page', () => {
  afterEach(() => {
    cleanup();
  });

  it('should render without exploding', () => {
    render(
      <MemoryRouter>
        <Page
          classes={{}}
          variables={{}}
          data={{ sublevel: { id: '1', name: 'name' }, products: [] }}
        />
      </MemoryRouter>
    );
  });
});
