import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import AddShoppingCart from '@material-ui/icons/AddShoppingCart';
import ItemComponent from 'Common/components/Item';
import useCart from 'Common/hooks/useCart';

const Item = ({ product }) => {
  const { addItem } = useCart();

  return (
    <ItemComponent product={product}>
      <IconButton color="secondary" onClick={() => addItem(product.id)}>
        <AddShoppingCart />
      </IconButton>
    </ItemComponent>
  );
};

Item.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isREquired,
    price: PropTypes.number.isRequired,
    available: PropTypes.bool.isRequired,
  }).isRequired,
};

export default Item;
