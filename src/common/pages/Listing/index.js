import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { handler } from 'Common/utils/query';
import Page from './Page';

const GET_LISTING = gql`
  query Listing(
    $id: ID!
    $query: String
    $available: Boolean
    $quantity: Int
    $min: Float
    $max: Float
    $order: ProductOrder
  ) {
    sublevel(id: $id) {
      id
      name
    }
    products(
      sublevel: $id
      query: $query
      available: $available
      quantity: $quantity
      min: $min
      max: $max
      order: $order
    ) {
      id
      quantity
      available
      price
      name
    }
  }
`;

const styles = {
  container: {
    flex: '1',
  },
};

export const Listing = ({ classes, location: { search }, match }) => {
  const params = queryString.parse(search);
  const variables = {
    id: match.params.sublevel,
  };

  if ('q' in params) variables.query = params.q;
  if ('available' in params) variables.available = params.available === 'true';
  if ('quantity' in params) variables.quantity = parseInt(params.quantity, 10);
  ['min', 'max'].forEach(opt => {
    if (opt in params) variables[opt] = parseFloat(params[opt]);
  });
  if (params.order) variables.order = params.order;

  return (
    <Grid container spacing={8} className={classes.container}>
      <Query query={GET_LISTING} variables={variables}>
        {handler(({ data }) => (
          <Page data={data} variables={variables} />
        ))}
      </Query>
    </Grid>
  );
};

Listing.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.object.isRequired,
  }).isRequired,
};

export default withStyles(styles)(Listing);
