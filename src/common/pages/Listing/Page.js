import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import withStyles from '@material-ui/core/styles/withStyles';
import MoodBadIcon from '@material-ui/icons/MoodBad';
import useRouter from 'Common/hooks/useRouter';
import Title from './Title';
import Breadcrumb from './Breadcrumb';
import Filter from './Filter';
import Item from './Item';
import Empty from 'Common/components/Empty';
import { Routes, buildURL } from 'Common/utils/url';

const styles = theme => ({
  wrapper: {
    margin: '0 auto',
    [theme.breakpoints.down('xs')]: {
      margin: `0 ${theme.spacing.unit}px`,
    },
  },
  order: {
    textAlign: 'right',
  },
  select: {
    minWidth: 120,
    marginLeft: theme.spacing.unit * 2,
  },
});

export const Page = ({ data, variables, classes }) => {
  const router = useRouter();

  return (
    <Grid className={classes.wrapper} xs={12} sm={11} md={10} lg={8} item>
      <Breadcrumb sublevel={data.sublevel} variables={variables} />
      <Title sublevel={data.sublevel} variables={variables} />
      <Grid container>
        <Grid item xs={12} md={4} lg={3}>
          <Filter sublevel={data.sublevel} variables={variables} />
        </Grid>
        <Grid item xs={12} md={8} lg={9}>
          <div className={classes.order}>
            Ordenar por:
            <Select
              className={classes.select}
              value={variables.order || ''}
              onChange={event => {
                const url = buildURL(Routes.LISTING, {
                  sublevel: data.sublevel,
                  ...variables,
                  order: event.target.value,
                });
                router.history.push(url);
              }}
            >
              <MenuItem value="PRICE_DESC">Mayor Precio</MenuItem>
              <MenuItem value="PRICE_ASC">Menor Precio</MenuItem>
              <MenuItem value="QUANTITY_DESC">Mayor Cantidad</MenuItem>
              <MenuItem value="QUANTITY_ASC">Menor Cantidad</MenuItem>
              <MenuItem value="AVAILABILITY_DESC">Disponible</MenuItem>
              <MenuItem value="AVAILABILITY_ASC">No Disponible</MenuItem>
            </Select>
          </div>
          {data.products.length ? (
            data.products.map(product => (
              <Item key={product.id} product={product} />
            ))
          ) : (
            <Empty Icon={MoodBadIcon}>
              Su b&uacute;squeda no tuvo resultados.
            </Empty>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

Page.propTypes = {
  data: PropTypes.shape({
    sublevel: PropTypes.object.isRequired,
    products: PropTypes.array.isRequired,
  }).isRequired,
  variables: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Page);
