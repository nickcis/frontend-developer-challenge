import React, { useState } from 'react';
import PropTypes from 'prop-types';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import withStyles from '@material-ui/core/styles/withStyles';
import { Routes, buildURL } from 'Common/utils/url';
import useRouter from 'Common/hooks/useRouter';

const styles = theme => ({
  form: {
    marginTop: theme.spacing.unit,
  },
  group: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    flex: '1',
  },
});

const Quantity = ({ classes, sublevel, variables }) => {
  const { quantity: initialQuantity, ...vars } = variables;
  const [quantity, setQuantity] = useState(
    typeof initialQuantity === 'undefined' ? '' : initialQuantity
  );
  const router = useRouter();

  return (
    <form
      className={classes.form}
      onSubmit={event => {
        event.preventDefault();
        const url = buildURL(Routes.LISTING, {
          sublevel,
          ...vars,
          quantity,
        });

        router.history.push(url);
      }}
      action={buildURL(Routes.LISTING, { sublevel })}
    >
      {Object.keys(vars).map(key => (
        <input key={key} type="hidden" name={key} value={vars[key]} />
      ))}
      <FormLabel component="legend">Cantidad</FormLabel>
      <div className={classes.group}>
        <TextField
          className={classes.input}
          name="quantity"
          value={quantity}
          onChange={event => setQuantity(event.target.value)}
        />
        <IconButton type="submit">
          <KeyboardArrowRight />
        </IconButton>
      </div>
    </form>
  );
};

Quantity.propTypes = {
  classes: PropTypes.object.isRequired,
  sublevel: PropTypes.object.isRequired,
  variables: PropTypes.object.isRequired,
};

export default withStyles(styles)(Quantity);
