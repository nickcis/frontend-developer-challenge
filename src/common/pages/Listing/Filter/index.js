import React from 'react';
import PropTypes from 'prop-types';
import Availability from './Availability';
import Price from './Price';
import Quantity from './Quantity';

const Filter = ({ sublevel, variables }) => (
  <React.Fragment>
    <Availability sublevel={sublevel} variables={variables} />
    <Price sublevel={sublevel} variables={variables} />
    <Quantity sublevel={sublevel} variables={variables} />
  </React.Fragment>
);

Filter.propTypes = {
  sublevel: PropTypes.object.isRequired,
  variables: PropTypes.object.isRequired,
};

export default Filter;
