import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import withStyles from '@material-ui/core/styles/withStyles';
import { Routes, buildURL } from 'Common/utils/url';

const LinkFormControlLabel = ({ to, ...props }) => (
  <Link to={to} style={{ textDecoration: 'none' }}>
    <FormControlLabel {...props} />
  </Link>
);

LinkFormControlLabel.propTypes = {
  to: PropTypes.string.isRequired,
};

const styles = theme => ({
  container: {
    width: '100%',
  },
  group: {
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'row',
      justifyContent: 'space-evenly',
    },
  },
});

const Availability = ({ classes, sublevel, variables }) => (
  <FormControl component="fieldset" className={classes.container}>
    <FormLabel component="legend">Disponibilidad</FormLabel>
    <RadioGroup
      className={classes.group}
      name="available"
      value={
        'available' in variables
          ? '' + variables.available
          : variables.available
      }
    >
      <LinkFormControlLabel
        to={buildURL(
          Routes.LISTING,
          (({ available, ...variables }) => ({ sublevel, ...variables }))(
            variables
          )
        )}
        control={<Radio />}
        label="Todos"
      />
      <LinkFormControlLabel
        to={buildURL(Routes.LISTING, {
          sublevel,
          ...variables,
          available: true,
        })}
        value="true"
        control={<Radio />}
        label="Disponible"
      />
      <LinkFormControlLabel
        to={buildURL(Routes.LISTING, {
          sublevel,
          ...variables,
          available: false,
        })}
        value="false"
        control={<Radio />}
        label="No disponible"
      />
    </RadioGroup>
  </FormControl>
);

Availability.propTypes = {
  sublevel: PropTypes.object.isRequired,
  variables: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Availability);
