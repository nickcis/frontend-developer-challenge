import React, { useState } from 'react';
import PropTypes from 'prop-types';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';
import { Routes, buildURL } from 'Common/utils/url';
import useRouter from 'Common/hooks/useRouter';

const styles = theme => ({
  form: {
    marginTop: theme.spacing.unit,
  },
  group: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: theme.spacing.unit,
  },
  first: {
    marginRight: theme.spacing.unit * 2,
  },
  input: {
    flex: '1',
  },
});

const Price = ({ classes, sublevel, variables }) => {
  const { min: initialMin, max: initialMax, ...vars } = variables;
  const [min, setMin] = useState(initialMin || '');
  const [max, setMax] = useState(initialMax || '');
  const router = useRouter();

  return (
    <form
      className={classes.form}
      onSubmit={event => {
        event.preventDefault();
        const url = buildURL(Routes.LISTING, {
          sublevel,
          ...vars,
          min,
          max,
        });

        router.history.push(url);
      }}
      action={buildURL(Routes.LISTING, { sublevel })}
    >
      {Object.keys(vars).map(key => (
        <input key={key} type="hidden" name={key} value={vars[key]} />
      ))}
      <FormLabel component="legend">Precio</FormLabel>
      <div className={classes.group}>
        <TextField
          className={classNames(classes.first, classes.input)}
          name="min"
          label="M&iacute;nimo"
          value={min}
          onChange={event => setMin(event.target.value)}
          InputProps={{
            startAdornment: <InputAdornment position="start">$</InputAdornment>,
          }}
        />
        <TextField
          className={classes.input}
          name="max"
          label="M&aacute;ximo"
          value={max}
          onChange={event => setMax(event.target.value)}
          InputProps={{
            startAdornment: <InputAdornment position="start">$</InputAdornment>,
          }}
        />
        <IconButton type="submit">
          <KeyboardArrowRight />
        </IconButton>
      </div>
    </form>
  );
};

Price.propTypes = {
  classes: PropTypes.object.isRequired,
  sublevel: PropTypes.object.isRequired,
  variables: PropTypes.object.isRequired,
};

export default withStyles(styles)(Price);
