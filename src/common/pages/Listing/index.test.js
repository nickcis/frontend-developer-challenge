import React from 'react';
import { render, cleanup } from 'react-testing-library';

const mockApollo = {
  Query: jest.fn(() => null),
};

jest.mock('react-apollo', () => mockApollo);

const { Listing } = require('.');

describe('Listing', () => {
  afterEach(() => {
    cleanup();
  });

  it('should render without exploding', () => {
    render(
      <Listing classes={{}} location={{ search: '' }} match={{ params: {} }} />
    );
  });
});
