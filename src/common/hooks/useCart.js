import { useContext } from 'react';
import CartContext from 'Common/contexts/Cart';

function useCart() {
  return useContext(CartContext);
}

export default useCart;
