import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import classNames from 'classnames';

const styles = theme => ({
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit * 6,
  },
});

const Footer = ({ classes, className }) => (
  <footer className={classNames(classes.footer, className)}>
    <Typography variant="h6" align="center" gutterBottom>
      El Barat&oacute;n
    </Typography>
    <Typography
      variant="subtitle1"
      align="center"
      color="textSecondary"
      component="p"
    >
      Siempre con las tres B: ¡Bueno, Bonito y Barato!
    </Typography>
  </footer>
);

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default withStyles(styles)(Footer);
