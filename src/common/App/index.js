import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from './Header';
import Footer from './Footer';
import withStyles from '@material-ui/core/styles/withStyles';
import { Route, Switch } from 'react-router-dom';
import Home from 'Common/pages/Home';
import Listing from 'Common/pages/Listing';
import Cart from 'Common/pages/Cart';

const styles = {
  '@global': {
    html: {
      height: '100%',
    },
    body: {
      height: '100%',
    },
    '#root': {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
  },
  content: {
    flex: '1 0 auto',
    display: 'flex',
    flexDirection: 'column',
  },
  main: {
    flex: '1',
    display: 'flex',
    flexDirection: 'column',
  },
  footer: {
    flexShrink: 0,
  },
};

const App = ({ classes }) => (
  <React.Fragment>
    <CssBaseline />
    <div className={classes.content}>
      <Header />
      <main className={classes.main}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/(.*)_sl:sublevel" component={Listing} />
          <Route exact path="/cart" component={Cart} />
          <Route component={() => 'Not found'} />
        </Switch>
      </main>
    </div>
    <Footer className={classes.footer} />
  </React.Fragment>
);

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
