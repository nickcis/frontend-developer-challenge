import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { Routes, buildURL } from 'Common/utils/url';
import useCart from 'Common/hooks/useCart';

const styles = theme => ({
  icon: {
    marginLeft: -theme.spacing.unit * 2,
  },
  link: {
    textDecoration: 'none',
    color: 'inherit',
  },
  grow: {
    flex: '1',
  },
});

const Header = ({ classes }) => {
  const home = buildURL(Routes.HOME);
  const { cart } = useCart();

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          className={classes.icon}
          color="inherit"
          component={({ className, ...props }) => (
            <Link
              to={home}
              className={classNames(classes.link, className)}
              {...props}
            />
          )}
        >
          <CameraIcon />
        </IconButton>
        <Typography variant="h6" color="inherit" noWrap>
          <Link to={home} className={classes.link}>
            El barat&oacute;n
          </Link>
        </Typography>
        <div className={classes.grow} />
        <IconButton component={Link} color="inherit" to={buildURL(Routes.CART)}>
          <Badge badgeContent={cart.length} color="secondary">
            <ShoppingCartIcon />
          </Badge>
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);
