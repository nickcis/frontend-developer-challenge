import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import yellow from '@material-ui/core/colors/yellow';
import cyan from '@material-ui/core/colors/cyan';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: yellow,
    accent: cyan,
    type: 'light',
  },
});

export default theme;
