import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CartContext from 'Common/contexts/Cart';

const CartKey = 'CART';

const CartProvider = ({ children }) => {
  const [cart, setCart] = useState([]);
  useEffect(() => {
    try {
      const cart = JSON.parse(window.localStorage.getItem(CartKey));

      if (cart.length) setCart(cart);
    } catch (e) {}
  }, []);

  const setCartAndStorage = cart => {
    setCart(cart);
    window.localStorage.setItem(CartKey, JSON.stringify(cart));
  };

  const removeItem = id => {
    setCartAndStorage(
      cart.reduce((cart, item) => {
        if (item.id !== id) {
          cart.push(item);
        } else if (item.amount > 1) {
          cart.push({
            ...item,
            amount: item.amount - 1,
          });
        }

        return cart;
      }, [])
    );
  };

  const addItem = id => {
    let hasToAdd = true;
    const newCart = cart.map(item => {
      if (item.id === id) {
        hasToAdd = false;
        return {
          ...item,
          amount: item.amount + 1,
        };
      }
      return item;
    });

    if (hasToAdd)
      newCart.push({
        id,
        amount: 1,
      });

    setCartAndStorage(newCart);
  };

  const clear = () => {
    setCartAndStorage([]);
  };

  return (
    <CartContext.Provider
      value={{
        cart,
        removeItem,
        addItem,
        clear,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

CartProvider.propTypes = {
  children: PropTypes.node,
};

export default CartProvider;
