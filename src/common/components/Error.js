import React from 'react';
import PropTypes from 'prop-types';
import ErrorIcon from '@material-ui/icons/Error';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  icon: {
    fontSize: theme.typography.h2.fontSize,
  },
  text: {
    marginTop: theme.spacing.unit * 2,
  },
});

const Error = ({ classes }) => (
  <div className={classes.container}>
    <ErrorIcon className={classes.icon} />
    <Typography variant="h5" component="span">
      No se pudo cargar, vuelva a intentar m&aacute;s tarde.
    </Typography>
  </div>
);

Error.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Error);
