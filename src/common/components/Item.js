import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  card: {
    margin: `${theme.spacing.unit * 2}px 0`,
    display: 'flex',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
  title: {
    textTransform: 'capitalize',
  },
  main: {
    flex: '1',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    [theme.breakpoints.down('xs')]: {
      height: 120,
    },
    [theme.breakpoints.up('sm')]: {
      minWidth: 160,
      width: '40%',
    },
  },
});

const Item = ({ classes, product, children }) => (
  <Card className={classes.card}>
    <CardMedia
      className={classes.cover}
      image={`https://loremflickr.com/320/240/${product.name}`}
      title={product.name}
    />
    <div className={classes.main}>
      <CardContent className={classes.content}>
        <Typography
          component="span"
          variant="h5"
          color="textSecondary"
          className={classes.title}
        >
          {product.name}
        </Typography>
        <Typography
          component="span"
          variant="subtitle2"
          color="textPrimary"
          gutterBottom
        >
          $ {product.price}
        </Typography>
        <Typography component="span" variant="body2" color="textSecondary">
          {`${product.quantity} unidades - ${
            product.available ? '' : 'no'
          } disponible`}
        </Typography>
      </CardContent>
      <CardActions>{children}</CardActions>
    </div>
  </Card>
);

Item.propTypes = {
  classes: PropTypes.object.isRequired,
  product: PropTypes.shape({
    name: PropTypes.string.isREquired,
    price: PropTypes.number.isRequired,
    available: PropTypes.bool.isRequired,
  }).isRequired,
  children: PropTypes.node.isRequired,
};

export default withStyles(styles)(Item);
