import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  container: {
    textAlign: 'center',
    color: theme.palette.text.hint,
    marginTop: theme.spacing.unit * 2,
  },
  icon: {
    fontSize: theme.typography.h2.fontSize,
  },
  text: {
    color: theme.palette.text.hint,
  },
});

const Empty = ({ classes, Icon, children }) => (
  <div className={classes.container}>
    <Icon className={classes.icon} />
    <Typography variant="h5" component="span" className={classes.text}>
      {children}
    </Typography>
  </div>
);

Empty.propTypes = {
  classes: PropTypes.object.isRequired,
  Icon: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default withStyles(styles)(Empty);
