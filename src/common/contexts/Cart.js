import React from 'react';

const CartContext = React.createContext({
  cart: [],
  addItem: () => {},
  removeItem: () => {},
  clear: () => {},
});

export default CartContext;
