FROM node:10.13.0-alpine

WORKDIR /opt/frontend-developer-challenge

COPY package.json yarn.lock ./

RUN yarn install

COPY . .

RUN yarn build

EXPOSE 3000

CMD [ "yarn", "start:prod" ]
