const pkg = require('./package.json');

module.exports = {
  extends: [
    'google',
    'plugin:react/recommended',
    'prettier',
    'prettier/react',
  ],
  parser: 'babel-eslint',
  plugins: [
    'babel',
    'react',
    'prettier',
  ],
  parserOptions: {
    ecmaVersion: 2016,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      experimentalObjectRestSpread: true
    },
  },
  settings: {
    react: {
      version: pkg.dependencies.react
    },
  },
  rules: {
    'prettier/prettier': ['error', {
      singleQuote: true,
      trailingComma: 'es5',
    }],
    'require-jsdoc': 'off',
    'no-invalid-this': 'off',
    'babel/no-invalid-this': 1,
  },
};
