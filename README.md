# Frontend Developer Challenge

Mi idea fue utilizar eso como una excusa para experimentar acerca de los siguientes temas:

- [GraphQL](https://graphql.org/)
- [Permitir que react funcione en client-side con JS deshabilitado](https://www.svtplay.se/klipp/16183939/reactjs-meetup-svt---english-subtitles)
- [React Hooks](https://reactjs.org/docs/hooks-state.html)
- [React testing library](https://github.com/kentcdodds/react-testing-library) (en vez de usar Enzyme para los tests)
- [React new Context API](https://reactjs.org/docs/context.html)

Intenté hacer un toy project saliendo del tipo stack de Redux + REST, etc (Si la idea era ver algo así, pueden ver [otro proyecto de prueba que hice hace más de un año](https://github.com/NickCis/radioactive-bird))

## Version en vivo!

Esta desplegado sobre [now!](https://zeit.co/now)

Pueden verlo en [https://frontend-developer-challenge.now.sh/](https://frontend-developer-challenge.now.sh/).

Por cuestiones del _free tier_, la instancia se autoescala a 0 instancias si no tiene actividad, lo que genera que la primera vez que se entre tarde más.

## Comandos

- `yarn start`: Iniciar en modo desarrollo
- `yarn format`: Ejecutar formateo de código automático con prettier
- `yarn lint`: Ejecutar lint
- `yarn build`: Compilar build de desarrollo
- `yarn start:prod`: Iniciar en modo producción (se debe correr `yarn build` previamente)
- `yarn deploy`: Desplegar a _now_

## Instalar

### Desde código fuente

```
$ yarn install
$ yarn build
$ yarn start:prod
```

Entrar a [http://localhost:3000](https://github.com/NickCis/radioactive-bird)

#### Con Docker

```
docker build -t frontend-developer-challenge .
docker run --rm -ti -p 3000:3000 frontend-develop-challenge
```

Entrar a [http://localhost:3000](https://github.com/NickCis/radioactive-bird)


### Imagen construida

El CI crea la imagen de docker y la sube al registro de Gitlab. Se puede bajar y utilizar dicha imagen ya construida.

```
docker run -ti --rm -p 3000:3000 registry.gitlab.com/nickcis/frontend-developer-challenge:latest
```

## Pipeline de desarrollo

Para el desarrollo de este proyecto se creo un pipeline muy sencillo. Se utiliza el CI de gitlab.

Este consta de 3 etapas:

- Build
  - Node: Genera el build de producción
  - Docker: Genera la imagen docker y la guarda en el registro de gitlab
- Test: Corre Lint y test unitarios
- Deploy: Despliega a Now el build de producción

Las etapas de _Build / Docker_ y _Deploy_ se corren solo para el branch master.

## Consideraciones

Se intentó sacarle provecho al ejercicio para experimentar con APIS nuevas o experimentales. Esta de más decir que muchas de las librerías utilizadas están en beta o alpha y no serían elecciones para un proyecto productivo.

Se utilizó un servicio para prober imágenes apartir de un texto, lo que hace que estas cambien (y generalmente tengan poco que ver con lo que se esta mostrando).

## Trabajo a Futuro

- No se realizó ninguna optimización relacionada con performance:
  - No se configuró Tree Shaking
  - Ni se hizo Code Splitting / Bundling (como se utilizan bastantes librerías: React, Apollo, Material UI, react router, etc), lo ideal sería separar en distintos bundles:
    - Manifest: Contiene código relacionado a webpack e información de bundles (cambia en cada release)
    - Vendor: Contiene librerías (cambia solo si se agregó o quitó alguna librería, lo ideal es cachearlo entre releases)
    - Pagina: Se genera uno por página (cambia solo sí se modificó la página, podría ser cacheado entre releases)
  - Habría que configurar un Service Worker para aplicar una estrategía agresiva de caching y preloading (los bundles tienen un hash de su contenido en su nombre, lo que facilita cachear de manera muy agresiva)
  - No se hizo ningún análisis de peso del bundle.
  - No se hicieron análisis de LightHouse.
  - No se aplican optimizaciones para memoizar valores en React.
- No se realizó ninguna mejora en relación a SEO:
  - No se optimizan OPFs de la página de listing
  - No se setean los metas correctamente por página (description, title, etc)
  - No se setean alt en imagenes
  - No se setean códigos de http correctamente (e.g.: 404 para un listing vacío)
- No se hicieron mejoras intensivas de UX: Hay errores de margenes, la paleta de colores no es la mejor que se podría haber elegido, la interfáz visual se podría haber pulido bastante más.
  - Home:
    - No se esta scrolleando al seleccionar la categoría, esta puede quedar fuera del viewpoint
  - Listing:
    - Los loaders son muy invasivos: Remueven toda la pantalla, lo mejor hubiese sido, usar placeholders, optimizar eveitar el refresco de cosas que no van a cambiar (h1, filtros, etc).
    - Sería mejor sería poder recorrer categorías, tener el input de search, por cuestión de simplicidad se dejó solo esto en la Home.
    - Comprar ítem debería mostrar mejor al usuario que esta pasando, solo lo agrega al carrito (visualmente solo se vé en el header el aumento del Badge
    - No se debería permitir comprar productos no disponibles
  - Cart:
    - Se muestra un spinner por prudcto en el carrito, algo bastante feo estéticamente.
    - Se debería pedir confirmación para eliminar un elemento del carrito, actualmente se lo elimina cuando se dejan 0 de estos items.
    - Se debería mostrar un mejor mensaje cuando salió la operación bien / hubo algún error
    - No se debería permitir finalizar la compra sin tener algo en el carrito, se podrían agregar validaciones de no dejar comprar productos no disponibles
- No se implmentó pantalla de pagos.
- No se utilizó ningún sistema de i18n: Lo recomendable sería separar el texto a mostrar de los componentes para poder utilizar textos localizados y/o en diferentes idiomas.
- No se desarrollaron test unitarios de manera intensiva: La mayoría de los Unit Tests son smoke tests, se debería enfatizar en probar el comportamiento de los componentes.
- No se realizó ningún test de integración: No se realizaron tests de la API ni end to end (e2e) test.
  - Para test e2e utilizaría [TestCafe](https://github.com/DevExpress/testcafe), aunque es un proyecto que aun no tiene públicada su versión 1.0, tiene una comunidad muy activa que responde rápida y están abiertos a contribuciones. Al utilizar el proyecto necesitaba de ciertas funcionalidades, al no estar implementadas mandé los PRs [1](https://github.com/DevExpress/testcafe/pulls?utf8=%E2%9C%93&q=is%3Apr+author%3ANickCis+) [2](https://github.com/DevExpress/testcafe-hammerhead/pulls?q=is%3Apr+author%3ANickCis).
- No se trabajó con un flow de desarrollo adecuado para equipos: ya que era un desarrollador solo, se hacían commits directos a _master_ con un equipo, se debería adoptar un flow adecuado (e.g.: Git Flow, Feature branch, etc)
- No se hizo incapié en mejorar los resolvers de GraphQL: se implementó una API que funciona, leyendo directamente de los archivos de productos y categorías, no se optimizaron los algoritmos de estas lecturas, filtrados, etc.
- No se implementó páginado: Implementar páginado en [GraphQL no es algo complicado](https://graphql.org/learn/pagination/) del lado del cliente sería solo agregar los links a las siguientes páginas.
- No se configuró ningún sistema de monitoreo (e.g.: NewRelic)
- No se configuró ningún sistema de trackeo de usuario (e.g.: Google Analytics)
